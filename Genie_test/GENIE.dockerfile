##########################################
#####   Docker Container recipe      #####
#####        for installing          #####
#####         GENIE3.02.02           #####
#####                                #####
#####        Creation date:          #####
#####       13 December 2021         #####
##########################################
#####                                #####
#####          Created by            #####
#####        Pedro Pasquini          #####
#####                                #####
##### inspirehep.net/authors/1467863 #####
#####                                #####
##########################################


FROM ubuntu:latest

## define defaut username
ARG myuser=pedro

RUN apt update; apt dist-upgrade -y

RUN apt-get update

## this is to avoid python asking timezones...
## a list of TZ can be found in: http://manpages.ubuntu.com/manpages/bionic/man3/DateTime::TimeZone::Catalog.3pm.html
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## Installing some important dependencies for ubuntu
RUN apt install -y dialog apt-utils dpkg-dev build-essential g++ gcc gfortran \
                   binutils pkgconf libtool cmake automake python3 python3-pip \
                   vim git wget python-is-python3

### Installing all packages needed for CERN ROOT
RUN apt install -y  libx11-dev libxpm-dev libtbb-dev\
                    libxft-dev libxext-dev libssl-dev libpcre3-dev \
                    xlibmesa-glu-dev libglew-dev libftgl-dev \
                    libmysqlclient-dev libfftw3-dev libcfitsio-dev \
                    graphviz-dev libavahi-compat-libdnssd-dev \
                    libldap2-dev python3-dev libxml2-dev libkrb5-dev \
                    libgsl0-dev libgsl-dev libtiff-dev libgif-dev libjpeg8-dev

### Installing some interesting python packages. NOTE numpy is needed for ROOT.
RUN pip3 install numpy matplotlib pygments pyyaml

### Creating a user. You can put your username below
RUN useradd -ms /bin/bash $myuser

WORKDIR /home/$myuser
ENV HOME=/home/$myuser

RUN mkdir -p $HOME/GENIE_INSTALLER
RUN mkdir -p $HOME/lib
RUN mkdir -p $HOME/include

ENV globfold=$HOME/GENIE_NEEDS

WORKDIR $globfold

#######################################
####### install log4cpp library #######
#######################################

## This will download log4cpp from sourceforge. You can download manually and add to the GENIE_NEEDS and comment this line.
RUN wget https://jaist.dl.sourceforge.net/project/log4cpp/log4cpp-1.1.x%20%28new%29/log4cpp-1.1/log4cpp-1.1.4.tar.gz -O log4cpp-1.1.4.tar.gz

RUN tar -xvf log4cpp-1.1.4.tar.gz

WORKDIR $globfold/log4cpp

RUN /bin/bash -c "./configure --prefix=$HOME"

RUN make 

RUN make install

WORKDIR $globfold

RUN rm -rf log4cpp
RUN rm log4cpp-1.1.4.tar.gz


######################################
####### install LHAPDF library #######
######################################

## This will download LHAPDF-6.5.4 from sourceforge. You can download manually and add to the GENIE_NEEDS and comment this line.
RUN wget https://lhapdf.hepforge.org/downloads/?f=LHAPDF-6.5.4.tar.gz -O LHAPDF-6.5.4.tar.gz

RUN tar -xvf LHAPDF-6.5.4.tar.gz
WORKDIR $globfold/LHAPDF-6.5.4
RUN /bin/bash -c "./configure --prefix=$HOME"
RUN make
RUN make install
WORKDIR $globfold
RUN rm LHAPDF-6.5.4.tar.gz
RUN rm -rf LHAPDF-6.5.4


## This will download CT10f4 from sourceforge. You can download manually and add to the GENIE_NEEDS and comment this line.
## You can also choose a different pdf if you prefer. See the list in https://lhapdf.hepforge.org/downloads?f=pdfsets/v6.backup/6.2
RUN wget https://lhapdfsets.web.cern.ch/current/CT10f4.tar.gz
RUN tar -xvf CT10f4.tar.gz
RUN cp -r $globfold/CT10f4/ $HOME/share/LHAPDF
WORKDIR $globfold
RUN rm CT10f4.tar.gz

#########################################
###### install pythia6.428 library ######
#########################################

ADD ./build_pythia6.sh ./build_pythia6.sh
RUN /bin/bash -c "source build_pythia6.sh"

RUN cp -a ./v6_428/lib/. $HOME/lib
RUN cp -a ./v6_428/inc/. $HOME/include/pythia6

WORKDIR $globfold
RUN rm -r v6_428
RUN rm build_pythia6.sh

WORKDIR $HOME 

####################################
####### install ROOT library #######
####################################

## This will download root. If you want to pass an existing folder instead
## comment the three lines below and uncomment the other
RUN git clone --branch latest-stable --depth=1 https://github.com/root-project/root.git root_src

## if you already have a root folder downloaded and want to pass it down, uncomment this 
## and comment previous lines
#ADD ./MyRoot/root $HOME/root

RUN mkdir -p $HOME/root
RUN mkdir -p $HOME/root_build

WORKDIR $HOME/root_build

RUN cmake -DCMAKE_INSTALL_PREFIX=$HOME/root $HOME/root_src \
          -Dpythia6=ON -DPYTHIA6_LIBRARY=$HOME/lib/libPythia6.so

RUN cmake --build . -- install -j8
RUN /bin/bash -c "source $HOME/root/bin/thisroot.sh"

ENV ROOTSYS $HOME/root
ENV PATH $ROOTSYS/bin:$PATH
ENV PYTHONPATH $ROOTSYS/lib:$PYTHONPATH
ENV CLING_STANDARD_PCH none

####################################
##### Finally installing GENIE #####
####################################

ENV GENIE $HOME/GENIEgenerator
ENV PYTHIA6 $HOME/lib

WORKDIR $HOME

## This will download GENIE. If you want to pass an existing folder instead
## you need to change things a bit.
RUN git clone https://github.com/GENIE-MC/Generator GENIEgenerator
WORKDIR $HOME/GENIEgenerator
RUN git checkout R-3_02_02

WORKDIR $GENIE

ENV LD_LIBRARY_PATH $LD_LIBRARY_PATH:$ROOTSYS/lib:$GENIE/lib:$PYTHIA6:$HOME/lib
ENV PATH=$PATH:$ROOTSYS/bin:$GENIE/bin 

RUN mkdir $HOME/GENIE

RUN ./configure --prefix=$HOME/GENIE --enable-t2k --enable-fnal --enable-atmo \
            --enable-lhapdf6 --disable-lhapdf5 --with-pythia6-lib=$HOME/lib/ \
            --with-lhapdf6-inc=$HOME/include --with-lhapdf6-lib=$HOME/lib/ \
            --with-log4cpp-inc=$HOME/include --with-log4cpp-lib=$HOME/lib/ \
            --with-libxml2-inc=/usr/include/libxml2 --with-libxml2-lib=/usr/share/aclocal\
            --enable-nucleon-decay --enable-nnbar-oscillation

RUN make 

RUN make install 

WORKDIR $HOME 

RUN cp -r GENIEgenerator/src $HOME/GENIE
RUN cp -r GENIEgenerator/data $HOME/GENIE
RUN cp -r GENIEgenerator/config $HOME/GENIE
RUN cp GENIEgenerator/VERSION $HOME/GENIE/VERSION

RUN rm -r $HOME/root_build 
RUN rm -r $HOME/root_src 
RUN rm -r $HOME/GENIE_NEEDS
RUN rm -r $HOME/GENIEgenerator

ENV GENIE $HOME/GENIE
ENV LD_LIBRARY_PATH $ROOTSYS/lib:$GENIE/lib:$PYTHIA6:$HOME/lib:$GENIE/src/Framework:$GENIE/src
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$GENIE/bin:$ROOTSYS/bin
ENV CPATH $GENIE/src/Framework:$GENIE/src/Physics:$GENIE/src/Tools:$GENIE/src:$HOME/include

USER $myuser

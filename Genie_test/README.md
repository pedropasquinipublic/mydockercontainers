________________________________________________
<div align="center">
GENIE 

Docker container for installation of GENIE3.02.02

Creation date: 13 December 2021      
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>

_______________________________________________

# Introduction
This is a docker file for easy installation instructions
of the GENIE library. Please cite the original references!! 
<ul>
[1] C. Andreopoulos, A. Bell, D. Bhattacharya, F. Cavanna, J. Dobson, S. Dytman, H. Gallagher, P. Guzowski, R. Hatcher and P. Kehayias,
''<em>The GENIE Neutrino Monte Carlo Generator</em>'',
<a href="http://dx.doi.org/10.1016/j.nima.2009.12.009">
Nucl. Instrum. Meth. A614, 87-104 (2010)</a>
[<a href="http://arxiv.org/abs/0905.2517">
arXiv:0905.2517</a> [hep-ph]].
</ul>
<ul>
[2] C. Andreopoulos, C. Barry, S. Dytman, H. Gallagher, T. Golan, R. Hatcher, G. Perdue and J. Yarba,
''<em>The GENIE Neutrino Monte Carlo Generator: Physics and User Manual</em>'',
[<a href="http://arxiv.org/abs/1510.05494">
arXiv:1510.05494</a> [hep-ph]].
</ul>


# Compiling the docker image
In order to compile the docker image, 
you will need docker and the lates 
Ubuntu on your docker file. 

Building the GENIE docker image

>> sudo docker build -t genie:3.02.02 -f GENIE.dockerfile .

To build from scrach you may add --no-cache
>> sudo docker build --no-cache -t genie:3.02.02 -f GENIE.dockerfile .

# Running the docker image
Starting Docker in bash mode

>> sudo docker run -it genie:3.02.02 bash


Starting Docker in bash mode with a folder to access

>> sudo docker run -it --volume ./myruns:$HOME/GENIEruns genie:3.02.02 /bin/bash



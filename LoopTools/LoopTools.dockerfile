##########################################
#####   Docker Container recipe      #####
#####        for installing          #####
#####         LoopTools              #####
#####                                #####
#####        Creation date:          #####
#####       21 December 2022         #####
##########################################
#####                                #####
#####          Created by            #####
#####        Pedro Pasquini          #####
#####                                #####
##### inspirehep.net/authors/1467863 #####
#####                                #####
##########################################

FROM ubuntu:latest

### Creating a user. You can put your username below
ENV myUser=pedro

RUN useradd -ms /bin/bash $myUser
WORKDIR /home/$myUser
ENV HOME=/home/$myUser

RUN apt update; apt dist-upgrade -y

RUN apt-get update

## this is to avoid python asking timezones...
## a list of TZ can be found in: http://manpages.ubuntu.com/manpages/bionic/man3/DateTime::TimeZone::Catalog.3pm.html
ENV TZ=America/Chicago
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

## Installing some important dependencies for ubuntu
RUN apt install -y dialog apt-utils dpkg-dev build-essential g++ gfortran \
                   binutils pkgconf libtool cmake automake python3-pip \
                   vim git wget


## There are two alternatives here:
### (i) You can manually download LoopTools (uncomment the line 44 and comment line 45).
### (ii) download LoopTools with wget (uncomment the line 45 and comment line 44).
#ADD LoopTools-2.16.tar.gz $HOME
RUN /bin/bash -c "wget https://feynarts.de/looptools/LoopTools-2.16.tar.gz"

RUN /bin/bash -c "tar -xf LoopTools-2.16.tar.gz"

WORKDIR $HOME/LoopTools-2.16

RUN mkdir $HOME/LoopTools

RUN /bin/bash -c "./configure --prefix=$HOME/LoopTools"

RUN /bin/bash -c "make"

RUN /bin/bash -c "make install"

ADD test $HOME/LoopTools/test

WORKDIR $HOME/LoopTools

RUN /bin/bash -c "mv -T lib64 lib"

ENV LT=$HOME/LoopTools
ENV PATH=$PATH:$LT/bin
ENV LIBRARY_PATH=$LIBRARY_PATH:$LT/lib
ENV CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$LT/include



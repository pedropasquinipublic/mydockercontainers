

#include <iostream>
#include "clooptools.h"


int main(int argc, char *argv[])
{ 

    ltini();
    double m1 = 7.071067811, m2 = 8.9442719099, m3 = 5.544;
    double p1 = 31.6227766, p2 = 25.278362;


    setmudim(1.0);

    std::cout << "B00 = " << B0i(bb0, p1*p1, m1*m1, m2*m2) << std::endl;
    std::cout << "B01 = " << B0i(bb1, p1*p1, m1*m1, m2*m2) << std::endl;


    // Notice the order of masses is different due to the momentum definition in LoopTolls 
    // See Section 1.3.4 on page 13.
    std::cout << "C000 = " << C0i(0, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;


    std::cout << "C100 = " << C0i(9, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;
    std::cout << "C001 = " << C0i(6, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;
    std::cout << "C010 = " << C0i(3, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;

    std::cout << "C011 = " << C0i(15, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;

    std::cout << "C020 = " << C0i(12, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;
    std::cout << "C002 = " << C0i(18, 0.0, p1*p1, p2*p2,  m1*m1, m2*m2, m3*m3) << std::endl;





    ltexi();
    return 0; 

}

________________________________________________
<div align="center">
LoopTools 

Docker container for installation of LoopTools

Creation date: 21 December 2022       
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>


________________________________________________

# Introduction
This is a docker file for easy installation instructions
of the LoopTools library. see <em><a href="https://feynarts.de/looptools/">Feynarts</a></em>. Please cite the original references!! 
<ul>
[1] T.~Hahn and M.~Perez-Victoria,
''<em>Automatized one loop calculations in four-dimensions</em>'',
<a href="http://dx.doi.org/10.1016/S0010-4655(98)00173-8">
Comput. Phys. Commun. 118 (1999)153</a>
[<a href="http://arxiv.org/abs/2208.02144">
arXiv:2201.10578</a> [hep-ph]].
</ul>
<ul>
[2] G.~J.~van Oldenborgh and J.~A.~M.~Vermaseren,
''<em>New Algorithms for One Loop Integrals</em>'',
<a href="http://dx.doi.org/10.1007/BF01621031">
Z. Phys. C46 (1990), 425-438</a>.
</ul>

# Compiling the docker image
In order to compile the docker image, 
you will need docker and the lates 
Ubuntu on your docker file. 

Building the LoopTools docker image

>> sudo docker build -t looptools:2.16 -f LoopTools.dockerfile .

To build from scrach you may add --no-cache
>> sudo docker build --no-cache -t looptools:2.16 -f LoopTools.dockerfile .

# Running the docker image
Starting Docker in bash mode

>> sudo docker run -it looptools:2.16 bash

________________________________________________
<div align="center">
My Docker Containers

Several docker containers for particle physics \& cosmology

Creation date: 13 December 2021       
Created by 

Pedro Pasquini      
<https:/inspirehep.net/authors/1467863>    
</div>


________________________________________________

# Introduction

This is a list of docker recipes for installation of several
physics codes that are somewhat hard to install for a beginer.
One can use it directly as a docker, or read the lines to 
figure out how to install directly in your PC.

# Instalation of Docker

In order to compile the docker image, you will need docker 
and the lates Ubuntu on your images.

## Installing docker

You may try directly 
>> sudo apt install docker-ce

Some times you need some dependencies or repositories
for installing docker, you can try

>>    sudo apt update

>>    sudo apt install apt-transport-https ca-certificates curl software-properties-common

>>    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

>>    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

>>    sudo apt update

>>    apt-cache policy docker-ce

Them you type
>> sudo apt install docker-ce

## Creating an Ubuntu image 
You need internet connection for this step.
Just type 

>> sudo docker pull ubuntu

# Building Docker Image from Recipe
The general command for creating a docker image is

>> sudo docker build -t name:version -f filename.dockerfile .

If some error occurs, docker will try to catch up where it left before. But if you want to build from scrach you may add --no-cache,

>> sudo docker build --no-cache -t name:version -f filename.dockerfile .

# Using the docker Image

If you want to use the image in bash mode just type
>> sudo docker run -it name:version bash

You might also want to point the docker to some folder in your computer containig, for example, cross-section files,
which are large and not convenient to add to the container. Simply type,

>> sudo docker run -it --mount "type=bind,source=$(pwd)/source/folder,target=/folder/at/container" name:version bash

You might want not to run docker as sudo. To do that, add your user to docker user list,

>> sudo usermod -aG docker \${USER}
>> su - ${USER}

_____

**<div align="center">  Warning! </div>**
    Docker sometimes do not close properly 
    and starts taking a lot of HD space.
    To remove it use the commands below 
_____

>>  docker container prune --force
>>  docker system prune

# List of Containers
<ol>
<li>LoopTools </li>
  <ul>
    See <em><a href="https://feynarts.de/looptools/">Feynarts</a></em>
    <ul>
    [1] T.~Hahn and M.~Perez-Victoria,
    ''<em>Automatized one loop calculations in four-dimensions</em>'',
    <a href="http://dx.doi.org/10.1016/S0010-4655(98)00173-8">
    Comput. Phys. Commun. 118 (1999)153</a>
    [<a href="http://arxiv.org/abs/2208.02144">
    arXiv:2201.10578</a> [hep-ph]].
    </ul>
    <ul>
    [2] G.~J.~van Oldenborgh and J.~A.~M.~Vermaseren,
    ''<em>New Algorithms for One Loop Integrals</em>'',
    <a href="http://dx.doi.org/10.1007/BF01621031">
    Z. Phys. C46 (1990), 425-438</a>.
    </ul>
  </ul>
<li> GENIE </li>
  <ul>
    See <em><a href="https://https://github.com/GENIE-MC">Genie-MC</a></em>
    <ul>
  <ul>
    [1] C. Andreopoulos, A. Bell, D. Bhattacharya, F. Cavanna, J. Dobson, S. Dytman, H. Gallagher, P. Guzowski, R. Hatcher and P. Kehayias,
    ''<em>The GENIE Neutrino Monte Carlo Generator</em>'',
    <a href="http://dx.doi.org/10.1016/j.nima.2009.12.009">
    Nucl. Instrum. Meth. A614, 87-104 (2010)</a>
    [<a href="http://arxiv.org/abs/0905.2517">
  arXiv:0905.2517</a> [hep-ph]].
  </ul>
  <ul>
    [2] C. Andreopoulos, C. Barry, S. Dytman, H. Gallagher, T. Golan, R. Hatcher, G. Perdue and J. Yarba,
    ''<em>The GENIE Neutrino Monte Carlo Generator: Physics and User Manual</em>'',
    [<a href="http://arxiv.org/abs/1510.05494">
    arXiv:1510.05494</a> [hep-ph]].
  </ul>
</ol>

#   Usefull docker commands 

1. Display all docker images
>> sudo docker images

2. Remove DOCKER image (-f -> force removal)
>> sudo docker rmi <IMAGE ID>

3. Run docker and open its terminal 

>> sudo docker run -it --volume path_to_folder:/folder/ name:version /bin/bash

4. log-out from bash opened docker: 
>> control-D

5. Go out without closing docker 
>> control-p + control-q 

6. Show all running dockers
>> sudo docker ps

7. copy docker file
>> sudo docker cp /home/pedro/my_dockers/globes-3.2.18/ dazzling_merkle:/neutrino_globes

8. Rename docker container
>> docker rename nome_original novo_nome

9. Detach the docker container
>> docker attach nome_docker

10. Add external folder to docker container
>> sudo docker cp ./folder dockertag:/newfolder

11. Run with directory attached:
>> docker run -t -i -v <host_dir>:<container_dir>  name /bin/bash

or

>> docker run -it --mount "type=bind,source=$(pwd)/folder_in,target=./folder_out" name:version bash

12. Get files from docker and vice-versa

First run docker:
>> docker run -p 4000:80 --name name dockername:latest

Then apply the command
>> sudo docker cp ./file_to_copy container_name:/new_file

13. You might want to stop a docker container:
>> docker stop name

14. List running container
>> sudo docker ps

list all quit containers 
>> sudo docker ps -q -a

15. Remove exited data from docker containers
>> sudo docker ps --filter 'status=exited' -q | xargs -r docker rm --force

16. Remove dangling images
>> sudo docker images -f 'dangling=true' -q | xargs -r docker rmi -f
  
  
